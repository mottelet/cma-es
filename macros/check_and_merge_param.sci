function param = check_and_merge_param(inparam, defparam)
// Checks input parameter list inparam w.r.t. valid fields:
// defparam defines all valid fields.
// Merges both parameter lists in that values from inparam
// are taken if available, otherwise from defparam.
// The function is recursively applied into lists and structs.
// Returns param == complemented parameter list

  if isempty(inparam)
    param = defparam;
    return;
  end
  names = getfield(1, inparam);
  names = names(2:$); // remove type/class name
  if typeof(inparam) == 'st' // remove also second entry of struct
    names = names(2:$)
  end
  defnames = getfield(1, defparam);
  defnames = defnames(2:$); // remove type/class name
  if typeof(defparam) == 'st' // remove also second entry of struct
    defnames = defnames(2:$)
  end

  param = defparam;
  // merge into param
  for name = names(1:$)
    // check
    if ~ or(name == defnames(1:$))
      error('unknown parameter fieldname: ' + name);
    // assign
    elseif ~isempty(inparam(name))
      if or(type(defparam(name)) == (15:17))
//pause
        param(name) = check_and_merge_param(inparam(name), defparam(name));
//pause
      else
        param(name) = inparam(name);
      end
    end
  end

endfunction
