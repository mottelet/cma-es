function cma_plotintern(name_prefix, fignb, name_extension, object_variables_name)
//
// plots data from CMA-ES, so far for Scilab and Java.
// defaults: name_prefix='outcmaes', name_extension='.xml'
//           fignb=326

// Can be called by Plot Now button, therefor the strange
// arrangement of input elements.

  defaults.name_prefix = 'outcmaes';
  defaults.name_extension = '.xml';
  defaults.object_variables = 'xmean'; // or 'xrecentbest'

  if ~isdef('name_prefix', 'local')
    name_prefix = defaults.name_prefix;
    if isdef('filenameprefix') // inherit from upper environment
      name_prefix = filenameprefix;
    end
  elseif isempty(name_prefix)
    name_prefix = defaults.name_prefix;
  elseif name_prefix == 1 // called from "Plot Now" button
    name_prefix = get(gcf(), "figure_name");
  end
  if ~isdef('name_extension', 'local')
    name_extension = defaults.name_extension;
  elseif isempty(name_extension)
    name_extension = defaults.name_extension;
  end
  if ~isdef('fignb', 'local')
    fignb = 326;
  end

  if ~isdef('object_variables_name', 'local')
    object_variables_name = defaults.object_variables;
  end

  cma_plot(fignb, name_prefix, name_extension, object_variables_name);

endfunction
