function flag = cma_stop(es)
  flag = ~isempty(es.out.stopflags);
endfunction
