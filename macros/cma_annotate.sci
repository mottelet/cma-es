function cma_annotate(xfinal, yfinal, ann)
// not in use but potentially useful
// INPUT
//      xfinal = xdata(idx($));   // single last abscissa value or two values (first and last)
//      yfinal = ydata(idx($),:); // y-values to be annotated
//      ann = string(condef(1,:)'); // row-wise strings to be printed

if length(xfinal) == 2
  xfirst = xfinal(1);
  xfinal = xfinal(2);
end
      // annotation "function", should become a stand alone function soon!?
      yrange = get(gca(), "y_ticks");
      yrange = yrange.locations([1 $]);
      yrange(1) = yrange(1) + 1e-6*(yrange(2) - yrange(1)); // prevents widening of range
      yrange(2) = yrange(2) - 1e-6*(yrange(2) - yrange(1));
      xrange = get(gca(), "x_ticks");
      xrange = xrange.locations([1 $]);
      xlast = max([1.07 * xfinal xrange(2)]);
      [ignore, idx] = gsort(yfinal);
      idx2 = [];
      Ngeno = length(yfinal);
      idx2(idx) = (Ngeno-1:-1:0)';
      plot(xfinal*ones(1,2), yrange, 'k', 'linewidth', 1);
      plot([xfinal; xlast]', ...
          [yfinal; ...
              yrange(1) + (idx2')*(yrange(2)-yrange(1))/(Ngeno-1)]);

      set(gca(), "clip_state", "off"); // numbers visible everywhere
      str = '';
      for i = 1:Ngeno
        if i > 9
          str = '';
        end
        xstring(xlast, yrange(1) + ...
            (yrange(2)-yrange(1))*(idx2(i)/(Ngeno-1) - 0.3/max(10,Ngeno)), ...
            [str ann(i)]);
      end

endfunction
