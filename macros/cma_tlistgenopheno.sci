function res = cma_tlistgenopheno(typical_x, scales, x0)
// TODO (not urgent as it only matters for the internal
//       representation of the typical solution):
//       redesign this in terms of typical_geno and typical_pheno
//       preferably top-down from input to cmaes.
//       typical_pheno==typical_x or x0 and
//       typical_geno = typical_geno when scales==1
// set up of an affine linear transformation using typical_x (default 0) for
//   translation and scales for scaling.
// typical_x is also used for the fixed variables,
//    otherwise x0 if typical_x is empty.
//
   if isempty(typical_x)
     typical_x = zeros(scales);
     typical_x(scales == 0) = x0(scales == 0);
   end
   skip  = 0;
   if and(typical_x == 0) & and(scales == 1)
     skip = 1;
   end
   res = tlist(['genopheno';'typical_x';'scaling';'skip'], ...
       typical_x, scales, skip);
endfunction
