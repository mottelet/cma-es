function es = cma_tell(es, X, arfitness)
  // X is a list of N-dimensional vectors
  //   (was a N x lambda array, a row vector of column vectors of size N)
  // arfitness is a row vector
  // both are transposed if the size is unique
  // this is not compliant to the OMD specification(s) (array of row vectors)

  N = es.N;

  // this is a bit unsave, as errors might occur later just by changing the problem dimension
  if size(arfitness, 1) == es.sp.lambda  & size(arfitness, 2) ~= es.sp.lambda
    arfitness = arfitness';
  end
  if 11 < 3 & size(X, 1) ~= N & size(X, 2) == N
    // X = X';
  end

  if length(X) ~= es.sp.lambda  // size(X, 1) ~= N | size(X, 2) ~= es.sp.lambda
    error('cma_tell: input X has wrong size, lambda vectors are expected');
  end
  if size(arfitness, 2) ~= es.sp.lambda
    error('cma_tell: arfitness must be a row vector of size lambda');
  end

  es.countiter = es.countiter + 1;
  es.counteval = es.counteval + es.sp.lambda;
  arx = [];

  for k=1:es.sp.lambda
    // arx(:,k) = cma_phenogenotransform(es.out.genopheno, X(:,k));
    if length(X(k)) ~= N
      error('vectors must have size ' + string(N))
    end
    arx(:,k) = cma_phenogenotransform(es.out.genopheno, X(k));
    if es.countiter <= es.sp.separable // diagonal CMA
      arz(:,k) = (arx(:,k)-es.xmean)./diag(es.D)/es.sigma; // == sigma^-1*D^-1*B'*(xmean-xold)
    else
      arz(:,k) = es.sigma^(-1)*diag(diag(es.D).^(-1))*es.B'*(arx(:,k)-es.xmean);   // == sigma^-1*D^-1*B'*(xmean-xold)
    end
  end

  // if or(abs(X(:,1) - es.xmean) > 10 * es.sigma * sqrt(diag(es.C)))
  if or(abs(arz) > 10)  // P(10-sigma) < 1e-20
    warning('input X is not consistent and will presumably lead to failure to converge');
  end

    // Sort by fitness and compute weighted mean into xmean
    [arfitness, arindex] = gsort(-arfitness); // minimization
    // this Matlab incompatibility sheds a dubious light on Scilab
    arfitness = - arfitness;
    es.fitnesshist = [arfitness(1); es.fitnesshist(1:$-1)];

    es.xmean = arx(:,arindex(1:es.sp.mu))*es.sp.weights;   // recombination, new mean value
    zmean = arz(:,arindex(1:es.sp.mu))*es.sp.weights;   // == sigma^-1*D^-1*B'*(xmean-xold)

    // Cumulation: Update evolution paths
    if es.countiter <= es.sp.separable // diagonal CMA
      es.ps   = (1-es.sp.cs)*es.ps + sqrt(es.sp.cs*(2-es.sp.cs)*es.sp.mueff) * (1 * zmean); // Eq. (4)
    else
      es.ps   = (1-es.sp.cs)*es.ps + sqrt(es.sp.cs*(2-es.sp.cs)*es.sp.mueff) * (es.B * zmean); // Eq. (4)
    end
    hsig = norm(es.ps) / sqrt(1-(1-es.sp.cs)^(2*es.counteval/es.sp.lambda))/es.const.chiN < 1.4 + 2/(N+1);
//qqq
//hsig = 1; disp('hsig=' + string(hsig));
    if es.countiter <= es.sp.separable // diagonal CMA
      es.pc   = (1-es.sp.cc)*es.pc + hsig * sqrt(es.sp.cc*(2-es.sp.cc)*es.sp.mueff) * (diag(es.D) .* zmean); // Eq. (2)
    else
      es.pc   = (1-es.sp.cc)*es.pc + hsig * sqrt(es.sp.cc*(2-es.sp.cc)*es.sp.mueff) * (es.BD * zmean); // Eq. (2)
    end

    // Adapt covariance matrix C
    if 1 < 3 & es.countiter <= es.sp.separable // diagonal CMA
      es.C = diag((1-es.sp.ccov) * diag(es.C) ...
          + es.sp.ccov * (1/es.sp.mucov) * (es.pc.^2 ...   // plus rank one update
                                            + (1-hsig) * es.sp.cc*(2-es.sp.cc) * diag(es.C)) ...
          + es.sp.ccov * (1-1/es.sp.mucov) ...           // plus rank mu update
                       * diag(es.C) .* (arz(:,arindex(1:es.sp.mu)).^2 * es.sp.weights));
    else
      es.C = (1-es.sp.ccov) * es.C ...                   // regard old matrix        // Eq. (3)
          + es.sp.ccov * (1/es.sp.mucov) * (es.pc*es.pc' ...   // plus rank one update
                                            + (1-hsig) * es.sp.cc*(2-es.sp.cc) * es.C) ...
          + es.sp.ccov * (1-1/es.sp.mucov) ...           // plus rank mu update
                       * (es.BD*arz(:,arindex(1:es.sp.mu))) ...
                       *  diag(es.sp.weights) * (es.BD*arz(:,arindex(1:es.sp.mu)))';
    end
    // Adapt step size sigma
    es.sigma = es.sigma * exp((es.sp.cs/es.sp.damps)*(norm(es.ps)/es.const.chiN - 1));  // Eq. (5)

    // Update B and D from C (without the modulo this is O(N^3))
    if es.countiter <= es.sp.separable // diagonal CMA
      es.C = diag(es.C); // for efficiency C is a diagonal vector for the next lines

      // Align order of magnitude of scales of sigma and C for nicer output
      if 1 < 2 & es.sigma > 1e10*sqrt(max((es.C)))
        fac = es.sigma/sqrt(median((es.C)));
        es.sigma = es.sigma/fac;
        es.pc = fac * es.pc;
        es.C = fac^2 * es.C;
      end

      es.D = sqrt(es.C); // D contains standard deviations now
      // es.D = es.D * N / sum(es.D);

      // set back to normal matrices
      es.BD = diag(es.D);
      es.D = diag(es.D);  // the output assumes matrices
      es.C = diag(es.C);

      if ceil(es.sp.separable) == es.countiter
        es.sp.ccov = es.sp.ccov_def;
      end

    // the original stuff
    elseif es.sp.ccov > 0 & modulo(es.countiter, 1/es.sp.ccov/N/5) < 1
      es.C     = triu(es.C)+triu(es.C,1)';  // enforce symmetry
      [es.B,es.D] = spec(es.C);             // eigen decomposition, B==normalized eigenvectors
      // error management
      if max(diag(es.D)) > 1e14*min(diag(es.D)) | min(diag(es.D)) <= 0
        if 11 < 3
          error('min(diag(es.D)) <= 0');
        elseif 1 < 3
          es.out.stopflags($+1) = 'conditioncov';
        else
          // limit condition of C to 1e14 + 1
          warning('condition number > 1e14 or min(diag(es.D)) = ' + ...
              string(min(diag(es.D))) + ' <= 0');
          tmp = max(diag(es.D))/1e14;
          es.C = es.C + tmp*eye(N,N); es.D = es.D + tmp*eye(N,N);
        end
      end
      es.D = diag(sqrt(diag(es.D))); // D contains standard deviations now
      es.BD = es.B*es.D;
    end

    // end implementation of core algorithm

    // Set stopflags, termination
    if (arfitness(1) <= es.param.stop.fitness)
      es.out.stopflags($+1) = 'fitness';
    end
    if es.counteval >= es.param.stop.maxfunevals
      es.out.stopflags($+1) = 'maxfunevals';
    end
    if es.countiter >= es.param.stop.maxiter
      es.out.stopflags($+1) = 'maxiter';
    end
    if and(es.sigma*(max(abs(es.pc), sqrt(diag(es.C)))) < es.param.stop.tolx)
      es.out.stopflags($+1) = 'tolx';
    end
    if or(es.sigma*sqrt(diag(es.C)) > es.param.stop.tolupx)
      es.out.stopflags($+1) = 'tolupx';
    end
    if max([es.fitnesshist(~isnan(es.fitnesshist)); arfitness']) ...
      - min([es.fitnesshist(~isnan(es.fitnesshist)); arfitness']) <= es.param.stop.tolfun
      es.out.stopflags($+1) = 'tolfun';
    end
    if es.countiter >= length(es.fitnesshist) & ...
      max(es.fitnesshist) - min(es.fitnesshist) <= es.param.stop.tolfunhist // <= makes a difference with zero
      es.out.stopflags($+1) = 'tolfunhist';
    end
    if max(diag(es.D)) / min(diag(es.D)) > 1e7 // captured above
//      for i = 1:N
//          es.C(i,i) = es.C(i,i) + max(diag(es.D))^2 / 1e14;
//      end
//      es.out.stopflags($+1) = 'conditioncov';
    end
    if or(es.xmean == es.xmean + 0.2*es.sigma*sqrt(diag(es.C)))
      es.out.stopflags($+1) = 'noeffectcoord';
    end

    if es.sp.separable <= es.countiter
      if or(es.xmean == es.xmean + 0.1*es.sigma*diag(es.D));
        es.out.stopflags($+1) = 'noeffectaxis';
      end
    else
      if and(es.xmean == es.xmean + 0.1*es.sigma*es.BD(:,1+floor(modulo(es.countiter,N))))
        es.out.stopflags($+1) = 'noeffectaxis';
      end
    end
    if max([es.fitnesshist; arfitness(1:ceil(1+es.sp.lambda/2))']) ...
        - min([es.fitnesshist; arfitness(1:ceil(1+es.sp.lambda/2))']) == 0
      es.out.stopflags($+1) = 'equalfunvals';
    end
    // read stopping message from file
    if ~isempty(es.param.verb.readfromfile)
      [fid, err] = mopen(es.param.verb.readfromfile, 'r');
      if err == 0
        [c s1 s3 idx] = mfscanf(fid, ' %s %s %s %i');
        if c > 1
          // 'stop' sets stopflags to manual
          if s1 == 'stop'
            es.out.stopflags($+1) = 'manual';
          end
        end
        mclose(fid);
      end // err == 0
    end // es.param.verb.readfromfile not empty

    es.stop = ~isempty(es.out.stopflags);

    // Keep overall best solution and some more data
    if es.countiter == 1
      es.out.solutions.bestever.x = cma_genophenotransform(es.out.genopheno, es.xmean);
      es.out.solutions.bestever.f = %inf;  // first hack
      es.out.solutions.bestever.evals = es.counteval;
    end

    fmean = %nan; // a simple hack to have fmean defined without additional feval
    es.out.evals = es.counteval;
    es.out.iterations = es.countiter;
    es.out.solutions.iterations = es.countiter;
    es.out.solutions.mean.x = cma_genophenotransform(es.out.genopheno, es.xmean);
    es.out.solutions.mean.f = fmean;
    es.out.solutions.mean.evals = es.counteval;
    es.out.solutions.recentbest.x = cma_genophenotransform(es.out.genopheno, arx(:, arindex(1)));
    es.out.solutions.recentbest.f = arfitness(1);
    es.out.solutions.recentbest.evals = es.counteval + arindex(1) - es.sp.lambda;
    es.out.solutions.recentworst.x = cma_genophenotransform(es.out.genopheno, arx(:, arindex($)));
    es.out.solutions.recentworst.f = arfitness($);
    es.out.solutions.recentworst.evals = es.counteval + arindex($) - es.sp.lambda;
    if arfitness(1) < es.out.solutions.bestever.f
      es.out.solutions.bestever.x = cma_genophenotransform(es.out.genopheno, arx(:, arindex(1)));
      es.out.solutions.bestever.f = arfitness(1);
      es.out.solutions.bestever.evals = es.counteval + arindex(1) - es.sp.lambda;
    end

    writeOutput(es, arx(:,arindex(1)), arfitness);

    // plot output data
    if es.param.verb.plotmodulo & ...
        (((es.countiter == 3 | ~modulo(1e-12*round(1e12*log10(es.countiter+1)),1))) ...
        | modulo(es.countiter-1,es.param.verb.plotmodulo) == 0 ...
        | ~isempty(es.out.stopflags))
      if es.countiter > 1
        cma_plotintern(es.param.verb.filenameprefix, 324);
      end
      if es.countiter == 2
        if isempty(strindex(get(gcf(), "figure_name"), ...
            es.param.verb.filenameprefix)) //"Plots of CMA-ES (Fig 324)"))
          // addmenu(324, 'Plot Now', 'plot now (refresh)', action=list(2, 'cma_plotintern'));
          set(gcf(), "figure_name", es.param.verb.filenameprefix);
        end
      end
    end

    // print / display

    if es.param.verb.displaymodulo & ...
        (es.countiter <= 3 | ~isempty(es.out.stopflags) | ...
         modulo(es.countiter-1,es.param.verb.displaymodulo) == 0)
      if modulo(es.countiter-1,10*es.param.verb.displaymodulo) == 0
        printf(es.verb.dispannotationline);
      end
      [Cmin, imin] = min(diag(es.C));
      [Cmax, imax] = max(diag(es.C));
      printf('%4d,%6d: %+14.7e +(%.0e)| %.2e |%3d:%.2e,%2d:%.2e\n', ...
          es.countiter, es.counteval, arfitness(1), arfitness($)-arfitness(1), ...
          max(diag(es.D))/min(diag(es.D)), ...
          imin, es.sigma*sqrt(Cmin), imax, es.sigma*sqrt(Cmax));
    end

endfunction // tell
