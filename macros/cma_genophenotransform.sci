function y = cma_genophenotransform(gp, x)
// input argument: tlist (an object), x vector to be transformed
// elements in gp :
//         typical_x = Nx1 vector of middle of domains, default == 0
//         scaling = Nx1 vector, default == 1
  if gp.skip
    y = x;
  else
    y = zeros(gp.typical_x); // only to set the size
    y(gp.scaling > 0) = x;
    y = gp.typical_x + y .* gp.scaling;
  end
endfunction
