function res = cma_minnan(in)

  res = min(in(~isnan(in)));

endfunction
