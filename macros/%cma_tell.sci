function es = %cma_tell(es, X, arfitness)
  // X is a list of N-dimensional vectors
  //   (was a N x lambda array, a row vector of column vectors of size N)
  // arfitness is a row vector
  // both are transposed if the size is unique
  // this is not compliant to the OMD specification(s) (array of row vectors)

  es = cma_tell(es,X,arfitness);

endfunction
