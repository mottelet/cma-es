function X = %cma_ask(es)
// returns a list with lambda N-dimensional candidate solutions
   X = cma_ask(es, es.sp.lambda);
endfunction
