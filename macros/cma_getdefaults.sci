function p = cma_getdefaults()
// returns struct param
// see function cma_new
//
  p = struct(); // the below might be slightly faster, but produces ugly output

//  p = tlist(['param', 'x0', 'typical_x', 'sigma0', 'opt_scaling_of_variables', ...
//      'stop_tolfun', 'stop_tolfunhist', 'stop_tolx', 'stop_tolupx', ...
//      'stop_fitness', 'stop_maxfunevals', 'stop_maxiter', 'opt_lambda', ...
//      'opt_mu', 'opt_separable', 'verb_logfunvals', 'verb_logmodulo', ...
//      'verb_displaymodulo', 'verb_plotmodulo', 'verb_readfromfile', ...
//      'verb_filenameprefix', 'readme']);

// reason for using stop_... instead of a struct: cma_new() displays everything, otherwise assignment is needed

  p.x0 = [];
  p.typical_x = [];
  p.sigma0 = [];
  p.opt.scaling_of_variables = [];

  p.stop.tolfun = 1e-12;
  p.stop.tolfunhist = 1e-12;
  p.stop.tolx = '1e-11*sigma0';
  p.stop.tolupx = '1e3*sigma0';
  p.stop.fitness = -%inf;
  p.stop.maxfunevals = %inf;
  p.stop.maxiter = '100*N+(N+5)^2*1e4/lambda'; // should depend on restarts as well

  p.opt.lambda = '4+floor(3*log(N))';
  p.opt.mu = 'lambda/2';
  p.opt.separable = 0; '5*N*10/lambda'; // not yet default, but in near future?

  // p.verb.silent = 0; // turn off all verbosity
  p.verb.logmodulo  = 1;        // writing output data
  p.verb.displaymodulo = 100;   // display
  p.verb.plotmodulo = 'max(logmodulo, 500)';   // only if output data are written
  p.verb.logfunvals = [];
  p.verb.readfromfile = 'signals.par'; // not really verbosity
  p.verb.filenameprefix = 'outcmaes';    //
  p.verb.append = 0             // overwrite old output files

  p.readme = [];
  p.readme.x0 = 'initial solution, either x0 or typical_x MUST be provided';
  p.readme.typical_x = 'typical solution, the genotypic zero for more convenient plots in future';
  p.readme.sigma0 = 'initial coordinate-wise standard deviation MUST be provided';
  p.readme.stop = 'termination options, see .stop.readme';
  p.readme.opt = 'more optional parameters, see .opt.readme';
  p.readme.verb = 'verbosity parameters, see .verb.readme';
  p.stop.readme.tolfun = 'stop if within-iteration function value differences are smaller';
  p.stop.readme.tolfunhist = 'stop if function value backward differences are smaller';
  p.stop.readme.tolx = 'stop if x-changes are smaller than tolx*sigma0*scaling';
                               // internally scaling must be omitted as it is part of GP-TF
  p.stop.readme.tolupx = 'stop if x-changes are larger than tolupx*sigma0*scaling';
  p.stop.readme.fitness = 'target objective function value (minimization)';
  p.stop.readme.maxfunevals = 'maximal number of function evaluations';
  p.stop.readme.maxiter = 'maximal number of iterations';
  p.opt.readme.scaling_of_variables = ...
      ['sigma0*scaling is in effect the initial standard deviation.' ...
       + ' scaling == 0 means that the variable does not take part in the optimization'];
  p.opt.readme.lambda = 'population size, number of sampled candidate solutions per iteration';
  p.opt.readme.mu = 'parent number, for experts only. Weighted recombination and mu=lambda/2 are default';
  p.opt.readme.separable = ['number of initial iterations with independent sampling for partly' ...
      + ' separable high-dimensional functions (n>100), 1==always'];
  // p.verb.readme.silent = 'if >0, turn off all verbosity';
  p.verb.readme.logmodulo = 'data logging, write every logmodulo iteration to file';
  p.verb.readme.displaymodulo = 'display every displaymodulo iteration some output to console';
  p.verb.readme.plotmodulo = 'call cma_plot() every plotmodulo iteration, only if logmodulo>0';
  p.verb.readme.logfunvals = 'record whenever these f-values are reached first';
  p.verb.readme.readfromfile = 'filename, ""stop"" as first line leads to manual termination';
  p.verb.readme.filenameprefix = 'output files';
  p.verb.readme.append = 'if true, start with Fevals=append and append files'

endfunction
