function x0 = cma_xstart(typical_x, x0, scales)
// computes xstart from the input parameters
// all in phenotypic space
  if ~isempty(x0)
    if typeof(x0) == 'string'
      x0 = evstr(x0);
    end
  else // generate x0 from typical_x
    if typeof(typical_x) == 'string'
      typical_x = evstr(typical_x);
    end
    // Rather no uniform distribution here, because of invariance
    x0 = typical_x + scales .* rand(scales,'normal');
  end
endfunction
