function X = cma_ask(es, lambda, mod)
// returns a list with lambda N-dimensional candidate solutions
// cma_tell only accepts a list of param.opts.lambda
// solutions, but parameter lambda is useful to resample
// single solution(s) conveniently. For lambda==1 one
// solution (not a list) is returned. With mod==1 always
// a list is returned.

  if ~isdef('lambda', 'local') | isempty(lambda) | lambda == 0
    lambda = es.sp.lambda
  end
  if ~isdef('mod', 'local') | isempty(mod)
    mod = 0;
  end

  // X = [];
  X = list();
  for k=1:lambda
    if es.countiter <= es.sp.separable // diagonal CMA
      x = es.xmean + es.sigma * (diag(es.D) .* rand(es.N, 1, 'normal'));
    else
      x = es.xmean + es.sigma * (es.BD * rand(es.N, 1, 'normal'));
    end
    // X(:,k) = cma_genophenotransform(es.out.genopheno, x);
    X(k) = cma_genophenotransform(es.out.genopheno, x);
  end
  if lambda == 1 & mod == 0
    X = X(1);
  end
endfunction
