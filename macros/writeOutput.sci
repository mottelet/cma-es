function writeOutput(es, xrecent, arfitness)
// writes data to four or five output files
//

    // Write output data to file
    if es.param.verb.logmodulo & (es.countiter < 10 ...
                 | (es.countiter < es.param.verb.logmodulo & modulo(log10(es.countiter),1) == 0) ...
                 | modulo(es.countiter-1,es.param.verb.logmodulo) == 0 ...
                 | ~isempty(es.out.stopflags) ...
                 | (~isempty(es.param.verb.logfunvals) & arfitness(1) <= es.param.verb.logfunvals(1)))

      es.param.verb.logfunvals(es.param.verb.logfunvals>=arfitness(1)) = [];

      // Save output data to files
      for name = es.files.additions
        [fid, err] = mopen(es.param.verb.filenameprefix+name+'.xml', 'a');
        if err ~= 0
          warning('could not open '+es.param.verb.filenameprefix+name+'.xml' + ' ');
        else
          if name == 'axlen'
            mfprintf(fid, '%d %d %e %e %e ', es.countiter, es.counteval, es.sigma, ...
                max(diag(es.D)), min(diag(es.D)));
            mfprintf(fid, msprintf('%e ', diag(es.D)) + '\n');
          elseif name == 'disp' // TODO
          elseif name == 'fit'
            mfprintf(fid, '%ld %ld %e %e %25.18e %25.18e %25.18e %25.18e', ...
                es.countiter, es.counteval, es.sigma, max(diag(es.D))/min(diag(es.D)), ...
                es.out.solutions.bestever.f, ...
                arfitness(1), median(arfitness), arfitness($));
// TODO contraints
//            if length(vecfitness(:,1)) > 1
//              mfprintf(fid, ' %25.18e', vecfitness(1:$, arindex(1)));
//            end
            mfprintf(fid, '\n');
          elseif name == 'stddev'
            mfprintf(fid, '%ld %ld %e 0 0 ', es.countiter, es.counteval, es.sigma);
//qqq            mfprintf(fid, (msprintf('%e ', es.sigma*es.out.genopheno.scaling.*sqrt(diag(es.C)))) + '\n');
            mfprintf(fid, (msprintf('%e ', es.sigma*sqrt(diag(es.C)))) + '\n');
          elseif name == 'xmean'
            if isnan(fmean)
              mfprintf(fid, '%ld %ld 0 0 0 ', es.countiter, es.counteval);
            else
              mfprintf(fid, '%ld %ld 0 0 %e ', es.countiter, es.counteval, fmean);
            end
//qqq            mfprintf(fid, msprintf('%e ', cma_genophenotransform(es.out.genopheno, es.xmean)) + '\n');
            mfprintf(fid, msprintf('%e ', es.xmean) + '\n');
          elseif name == 'xrecentbest'
            mfprintf(fid, '%ld %ld %25.18e 0 0 ', es.countiter, es.counteval, arfitness(1));
            mfprintf(fid, msprintf('%e ', ...
                cma_genophenotransform(es.out.genopheno, xrecent)) + '\n');
          end
          mclose(fid);
        end
      end // for
    end // if logmodulo
endfunction
