function x = cma_phenogenotransform(gp, x)
// input argument: tlist (or mlist) according to Gilles Pujol
  if ~gp.skip
    idx = gp.scaling > 0;
    x = (x(idx) - gp.typical_x(idx)) ./ gp.scaling(idx);
  end
endfunction
