function scales = cma_check_initial_sizes(typical_x, x0, scales)
// checks sizes of x0, typical_x, scales (can be a scalar),
// checks that scales >= 0,
// returns Nx1 vector scales
// inputs must be defined but can be empty, if appropriate.

  // --- extract typical_x
  if ~isempty('typical_x') & ~sum(size(typical_x)) == 0
    if typeof(typical_x) == 'string'
      typical_x = evstr(typical_x);
    end
  else // x0 must be defined
    typical_x = zeros(x0);
  end

  // --- check typical_x
  N = size(typical_x, 1);
  if ~and(size(typical_x) == [N 1]) | N == 1
    error('CMAES: typical_x or x0 must be a column vector');
  end

  // --- extract and check x0
  if ~isempty('x0') & typeof(x0) == 'string'
    x0 = evstr(x0);
    if ~and(size(x0) == [N 1])
      error('CMAES: x0 must be a column vector and agree with typical_x');
    end
  end

  // --- extract and check scales
  if isempty(scales)
    scales = ones(N,1);
  elseif typeof(scales) == 'string'
    scales = evstr(scales);
  end
  if 11 < 3 & and(size(scales) == [1 1]) // sigma is used as initial step-size
    scales = scales * ones(N,1);
  end
  if ~and(size(scales) == [N 1])
    error('CMAES: input parameter scales must be a column vector and agree with x0 and typical_x');
  end
  if or(scales < 0)
    error('CMAES: input parameter scales must >= zero');
  end

endfunction
